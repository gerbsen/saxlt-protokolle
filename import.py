#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import sys
import io
import textract
import re
import numpy
import glob
import json
from sklearn.feature_extraction.text import CountVectorizer
import os
from whoosh.index import create_in
from whoosh.fields import *

# source: https://tim-ehling.com/deutsche-stoppwort-liste-400/
stop_words = ["man",  "im", "nicht", "noch", "ab", "aber", "abgesehen", "alle", "allein", "aller", "alles", "als", "am", "an", "andere", "anderen", "anderenfalls", "anderer", "anderes", "anstatt", "auch", "auf", "aus", "aussen", "außen", "ausser", "außer", "ausserdem", "außerdem", "außerhalb", "ausserhalb", "behalten", "bei", "beide", "beiden", "beider", "beides", "beinahe", "bevor", "bin", "bis", "bist", "bitte", "da", "daher", "danach", "dann", "darueber", "darüber", "darueberhinaus", "darüberhinaus", "darum", "das", "dass", "daß", "dem", "den", "der", "des", "deshalb", "die", "diese", "diesem", "diesen", "dieser", "dieses", "dort", "duerfte", "duerften", "duerftest", "duerftet", "dürfte", "dürften", "dürftest", "dürftet", "durch", "durfte", "durften", "durftest", "durftet", "ein", "eine", "einem", "einen", "einer", "eines", "einige", "einiger", "einiges", "entgegen", "entweder", "erscheinen", "es", "etwas", "fast", "fertig", "fort", "fuer", "für", "gegen", "gegenueber", "gegenüber", "gehalten", "geht", "gemacht", "gemaess", "gemäß", "genug", "getan", "getrennt", "gewesen", "gruendlich", "gründlich", "habe", "haben", "habt", "haeufig", "häufig", "hast", "hat", "hatte", "hatten", "hattest", "hattet", "hier", "hindurch", "hintendran", "hinter", "hinunter", "ich", "ihm", "ihnen", "ihr", "ihre", "ihrem", "ihren", "ihrer", "ihres", "ihrige", "ihrigen", "ihriges", "immer", "in", "indem", "innerhalb", "innerlich", "irgendetwas", "irgendwelche", "irgendwenn", "irgendwo", "irgendwohin", "ist", "jede", "jedem", "jeden", "jeder", "jedes", "jedoch", "jemals", "jemand", "jemandem", "jemanden", "jemandes", "jene", "jung", "junge", "jungem", "jungen", "junger", "junges", "kann", "kannst", "kaum", "koennen", "koennt", "koennte", "koennten", "koenntest", "koenntet", "können", "könnt", "könnte", "könnten", "könntest", "könntet", "konnte", "konnten", "konntest", "konntet", "machen", "macht", "machte", "mehr", "mehrere", "mein", "meine", "meinem", "meinen", "meiner", "meines", "meistens", "mich", "mir", "mit", "muessen", "müssen", "muesst", "müßt", "muß", "muss", "musst", "mußt", "nach", "nachdem", "naechste", "nächste", "nebenan", "nein", "nichts", "niemand", "niemandem", "niemanden", "niemandes", "nirgendwo", "nur", "oben", "obwohl", "oder", "oft", "ohne", "pro", "sagte", "sagten", "sagtest", "sagtet", "scheinen", "sehr", "sei", "seid", "seien", "seiest", "seiet", "sein", "seine", "seinem", "seinen", "seiner", "seines", "seit", "selbst", "sich", "sie", "sind", "so", "sogar", "solche", "solchem", "solchen", "solcher", "solches", "sollte", "sollten", "solltest", "solltet", "sondern", "statt", "stets", "tatsächlich", "tatsaechlich", "tief", "tun", "tut", "ueber", "über", "ueberall", "überll", "um", "und", "uns", "unser", "unsere", "unserem", "unseren", "unserer", "unseres", "unten", "unter", "unterhalb", "usw", "viel", "viele", "vielleicht", "von", "vor", "vorbei", "vorher", "vorueber", "vorüber", "waehrend", "während", "wann", "war", "waren", "warst", "wart", "was", "weder", "wegen", "weil", "weit", "weiter", "weitere", "weiterem", "weiteren", "weiterer", "weiteres", "welche", "welchem", "welchen", "welcher", "welches", "wem", "wen", "wenige", "wenn", "wer", "werde", "werden", "werdet", "wessen", "wie", "wieder", "wir", "wird", "wirklich", "wirst", "wo", "wohin", "wuerde", "wuerden", "wuerdest", "wuerdet", "würde", "würden", "würdest", "würdet", "wurde", "wurden", "wurdest", "wurdet", "ziemlich", "zu", "zum", "zur", "zusammen", "zwischen"]
stop_words += ["albrecht", "alexander", "aloysius", "andrea", "andreas", "andré", "anna", "antje", "anton", "antonia", "barbara", "barth", "beger", "breitenbuch", "brünler", "buddeberg", "böhme", "cagalj","sejdi", "carsten", "christian", "christiane", "christin", "christopher", "claudia", "daniel", "daniela", "dierks", "dietmar", "dietrich", "dirk", "dombois", "doreen", "dornau", "dr", "dringenberg", "dulig", "eric", "feiks", "firmenich", "flemming", "frank", "franz", "franziska", "friedel", "fritzsche", "gahler", "gasse", "gebhardt", "geert", "gemkow", "georg", "ludwig", "gerald", "gerber", "gerhard", "gorskih", "gudrun", "hahn", "hammecke", "hanka", "hans", "jürgen", "hartmann", "hein", "heinz", "henning", "hentschel", "hermann", "hippold", "holger", "homann", "hösl", "hütter", "ines", "ingo", "iris", "ivo", "jan", "jan", "oliver", "jens", "joachim","michael", "jost", "juliane", "jörg", "steffen", "kathleen", "kay", "keil", "keiler", "keller", "kerstin", "kiesewetter", "kirmes", "kirste", "klepsch", "kliese", "kretschmer", "kuge", "kuhfuß", "kuhnert", "kummer", "kumpf", "kuppi", "köditz", "kühne", "lang", "lars", "leithoff", "liebscher", "lippmann", "lucie", "luise", "lupart", "löffler", "löser", "mackenroth", "maicher", "mann", "marco", "marika", "mario", "markert", "marko", "martin", "martina", "matthias", "mayer", "melcher", "mertsching", "meyer", "michael", "mikwauschk", "mirko", "modschiedler", "nagel", "neuhaus", "wartenberg", "nico", "nicolaus", "norbert","otto", "nowak", "oberhoffner", "oliver", "otto", "pallas", "panter", "patricia", "patt", "penz", "peschel", "peter", "petra", "petzold", "piwarz", "pohle", "prantl", "prof", "rene", "richter", "rico", "ritter", "roberto", "rohwer", "roland", "rolf", "romy", "ronald", "ronny", "rost", "rößler", "sabine", "saborowski", "sarah", "schaper", "schaufel", "schenderlein", "schiemann", "schmidt", "schreyer", "schubert", "schultze", "schwietzer", "sebastian", "simone", "sodann", "springer", "stephan", "susan", "susanne", "svend", "gunnar", "sören", "teichmann", "thomas", "thumm", "timo", "tobias", "torsten", "tändler", "walenta", "ulbrich", "ulrich","willi", "urban", "valentin", "voigt", "volker", "volkmar", "walter", "weigand", "wendt", "wiesner", "wilhelm", "winkler", "wippel", "wissel", "wolf", "dietrich", "wolfram", "wähner", "wöller", "zickler", "zschocke", "zwerg"]
stop_words += ["herr", "jetzt", "vielen", "damit", "einmal", "herren", "damen", "gibt", "schon", "wollen", "ja", "keine", "denn", "er", "dafür", "möchte", "dazu", "ganz", "nun", "gerade", "doch", "bereits", "genau", "ob", "also", "geehrte", "kolleginnen", "kollegen", "natürlich", "dabei", "letzten", "vom", "darauf", "kein"]

# bug converting numpy int64 into json workaround 
def convert(o):
    if isinstance(o, numpy.int64): return int(o)  
    raise TypeError


schema = Schema(filename=TEXT(stored=True), \
  legislatur=NUMERIC(stored=True), \
  date=ID(stored=True), \
  meetingID=ID(stored=True), \
  meetingType=ID(stored=True), \
  content=TEXT(stored=True))
ix = create_in("/saxlt/index", schema)
writer = ix.writer()

for filePath in [f for f in glob.glob("/saxlt/data/*.pdf")]:
  text = textract.process(filePath).decode("utf8")
  # write the result to a plain text file
  newFile = open(filePath.replace("pdf", "txt"), "w")
  newFile.write(text)
  newFile.close()
    
  # metadata
  startIndex = text.find("Beginn: ") + 8
  endIndex = text.find("Schluss: ") + 9
  dateStartIndex = 88 + text[87:120].find(" ")
  dateEndIndex = dateStartIndex + text[dateStartIndex:120].find(",")
  result = {
    'filename' : filePath.replace("/saxlt/data/", ""),
    'datum' : text[dateStartIndex:dateEndIndex],
    'sitzungsnummer' : int(text[18:text.find("\n", 17)]),
    'legislatur' : int(text[16:17]),
    'start' : text[startIndex:startIndex+9],
    'end' : text[endIndex:endIndex+9],
    'type' : 'plenarprotokoll' if "7_PlPr" in filePath and ".pdf" in filePath else "unknown"
  }

  # write text to index
  writer.add_document(
    filename=result['filename'], \
    legislatur=result['legislatur'], \
    date=result['datum'], \
    meetingID=str(result['sitzungsnummer']), \
    meetingType=result['type'], \
    content=text)

  # n-grams and statistics
  for n in (1,2,3):
    countVectorizer = CountVectorizer(stop_words=stop_words, analyzer='word', ngram_range=(n,n))
    count_train     = countVectorizer.fit([open(filePath.replace("pdf", "txt")).read()])
    bag_of_words    = countVectorizer.transform([open(filePath.replace("pdf", "txt")).read()])
    word_list       = countVectorizer.get_feature_names();    
    count_list      = bag_of_words.toarray().sum(axis=0)
    occurrences     = dict(zip(word_list,count_list))
    sort_orders     = sorted(occurrences.items(), key=lambda x: x[1], reverse=True)
    
    result[str(n) + "_gram_occurrences"] = occurrences
  
  # write output file
  with io.open(filePath.replace("pdf", "json"), mode="w", encoding="UTF8") as newFile:
    newFile.write(json.dumps(result, default=convert, ensure_ascii=False, indent=2))
    newFile.close()
  
writer.commit()