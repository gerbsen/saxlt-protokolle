#!/usr/bin/env python
# encoding: utf-8
import query as saxlt
import graphs
import sys
import json
import nlp
import time
from flask import Flask
from flask import make_response
from flask import render_template
from flask import request
from flask import jsonify
from flask_caching import Cache
from whoosh.qparser import QueryParser
from whoosh.index import open_dir
from logging.config import dictConfig
from flask.logging import create_logger

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

# if __name__ == "__main__":
app = Flask(__name__)
cache = Cache(app, config={'CACHE_TYPE': 'simple'})
log = create_logger(app)

app.config['ENV'] = 'development'
app.config['DEBUG'] = True
app.config['TESTING'] = True
app.config['TEMPLATES_AUTO_RELOAD'] = True

def before_request():
    app.jinja_env.cache = {}
app.before_request(before_request)

@app.route('/display-document')
@cache.cached(timeout=50)
def display_document():
  lines = None
  documentInJson = None
  with open('/saxlt/data/' + request.args.get('filename').replace(".pdf", ".txt")) as f:
    lines = list(f)
    request.args.get('filename')
  with open('/saxlt/data/' + request.args.get('filename').replace(".pdf", ".json")) as f:
    documentInJson=json.loads(f.read())
  
  lines = [nlp.highlight_text(line, request.args.get('searchString')) for line in lines]
  return render_template('display-document.html', \
    lines=lines,
    numberOfMatches=sum(1 for s in lines if 'highlighted-match' in s), \
    meetingID=documentInJson['sitzungsnummer'], \
    meetingDate=documentInJson['datum'], \
    matchingLines=nlp.find_matching_lines(lines))

@app.route('/')
@app.route('/index')
@cache.cached(timeout=50)
def index():
  
  content = {}
  start = time.time()
  content['CORONA_OCCURRENCES_SPEC'] = saxlt.toHTML(graphs.getGraphDefinitions()[0])
  content['ZURUF_SPEC'] = saxlt.toHTML(graphs.getGraphDefinitions()[1])
  content['GEFUEHL_SPEC'] = saxlt.toHTML(graphs.getGraphDefinitions()[2])
  content['column_names'] = ['Index', 'Häufigkeit', 'Wort(gruppe)']
  end = time.time()
  log.info("It took %.2fms to generate the graphs.", end - start)
  
  start = time.time()
  occs = saxlt.getTopOccurrences(250, 0, ['plenarprotokoll'])
  content['gram1_values'] = occs['1gram'].to_dict(orient='index')
  content['gram2_values'] = occs['2gram'].to_dict(orient='index')
  content['gram3_values'] = occs['3gram'].to_dict(orient='index')
  end = time.time()
  log.info("It took %.2fms to generate the top n occurrences.", end - start)
  
  # this is for some reason not working, it somehow puts "('" before the value of the 
  # result: {'CORONA_OCCURRENCES_SPEC': ('{\n  "$schema":
  # for definition in graphs.getGraphDefinitions():
  #   content[definition['id']] = saxlt.toHTML(definition),

  return render_template('dashboard.html', **content)

@app.route('/graph')
def graph():
  return saxlt.compare_graph_from_vega_json(request.args.getlist('word'))

@app.route('/compare')
def compare():
  return make_response(saxlt.compare(request.args.getlist('word')).to_json())

@app.route('/search')
def search():
  ix = open_dir("/saxlt/index")
  with ix.searcher() as searcher:
    searchString  = request.args.get('query').lower()
    contextLength = int(request.args.get('contextLength')) or 2
    query = QueryParser("content", ix.schema).parse(searchString)
    log.info('Query: "%s" was send to the server', query)
    results = []
    i = 0
    for res in searcher.search(query):

      # log.info('Hit: "%s" "%s"  ', res['filename'], res.highlights("content"))

      findings = nlp.get_window(res['content'].lower(), searchString, contextLength)
      for occurrence in findings:
        results.append({
          'id': i,
          'filename' : res['filename'],
          'occurrence' : occurrence,
          'count': len(findings),
          'meetingDate': res['date'],
          'meetingID': res['meetingID'],
          'meetingType': res['meetingType'],
          'foundForString' : searchString
        })
        i += 1

    return jsonify({ 'results' : results, 'query' : request.args.get('query'), 'contextLength' : contextLength })

@app.route('/top-occurrences')
def getTopOccurrences():
  top     = int(request.args.get('top')) or 25
  offset  = int(request.args.get('offset')) or 0
  doctype = request.args.getlist('doctype') or ['plenarprotokoll']
  result  = saxlt.getTopOccurrences(top, offset, doctype)

  for key, value in result.items():
    result[key] = value.to_dict(orient='index')

  return jsonify(result)

app.run(host='0.0.0.0', port=5000)