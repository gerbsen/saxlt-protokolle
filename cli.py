#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import query
import glob, sys
import json
import argparse
import pandas
import altair as altair
from collections import Counter

parser = argparse.ArgumentParser(description='Anfrage an alle Plenarprotokolle.')
parser.add_argument('--word', metavar='Wort', type=str, nargs='+', help='Ein oder mehrere Worte nach dem/denen gesucht werden soll. Synonyme werden mit "|" abgetrennt geschrieben: "links|rechts"')
parser.add_argument('--format', metavar='Format', type=str, help='Format für die Ausgabe, default ist CSV', default='csv', choices=['csv', 'json'])
parser.add_argument('--doctype', metavar='Dokumententype', type=str, nargs='+', help='Der Typ des zu durchsuchenden Dokuments', default=['plenarprotokoll'], choices=['plenarprotokoll'])
parser.add_argument('--query', metavar='Anfragetyp', type=str, help='Welcher Typ Anfrage soll ausgeführt werden.', default='word', choices=['word', 'top-occ', 'compare'])
parser.add_argument('--top', metavar='Top n Werte', type=int, help='Wieviele Werte sollen ausgegeben werden.', default=25, choices=range(0, 100000000000))
parser.add_argument('--ngram', metavar='Welche n-gram', type=int, help='Welche n-gram Serie soll untersucht werden.', default=1, choices=[1,2,3])
parser.add_argument('--offset', metavar='Offset von Top-n Werte', type=int, help='Wieviele Werte sollenam Anfang übersprungen werden.', default=0)
args = parser.parse_args()

def queryWord(words, format='json'):
  if format == 'json':
    print("Suchwörter: " + str(words))
    print(json.dumps(query.getOccurrences(words), indent=2))
  elif format == 'csv':
    print("Suchwörter: " + str(words))
    print(pandas.DataFrame(query.getOccurrences(words)).to_csv(index=False))
  else:
    print('Format not supported')

if args.query == 'word':
  queryWord(args.word, args.format)
elif args.query == 'top-occ':
  result = query.getTopOccurrences(args.top, args.offset, args.doctype)
  print(result['1gram'])
  print(result['2gram'])
  print(result['3gram'])
elif args.query == 'compare':
  if ( args.word is None or len(args.word) <= 1):
    sys.exit("Need at least two words to compare: " + str(args.word))
  print(query.compare(args.word))
else:
  print("Falscher Parameter")

