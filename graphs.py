def getGraphDefinitions():
  return [
    {
    "id"   : "CORONA_OCCURRENCES_SPEC",
    "title": "Häufigkeit der Wörter „Corona”, „Covid”, „Covid-19” in Plenarprotokollen",
    "type" : "word",
    "words": set(["Corona", "Covid", "Covid-19", "coronavirus"]),
    "x": {
      "title": "Sitzungsnummer",
      "id": "sitzungsnummer"
    },
    "y": {
      "title": "Anzahl des Vorkommens",
      "id": "count"
    }
  },{
    "id"   : "ZURUF_SPEC",
    "title": "Häufigkeit des Wörtes „Zuruf” in Plenarprotokollen",
    "type" : "word",
    "words": set(["Zuruf"]),
    "x": {
      "title": "Sitzungsnummer",
      "id": "sitzungsnummer"
    },
    "y": {
      "title": "Anzahl des Vorkommens",
      "id": "count"
    }
  },
  {
    "id"   : "GEFUEHL_SPEC",
    "title": "TODO",
    "type" : "compare",
    "words": ["angst", "vertrauen", "zuversicht"],
    "x": {
      "title": "Sitzungsnummer",
      "id": "sitzungsnummer"
    },
    "y": {
      "title": "Anzahl des Vorkommens",
      "id": "value"
    }
  }]