#!/bin/bash

# this file contains all commands that are run/shown in the Readme.md 
# this should be run before commiting so that potential breaks in the API/CLI are catched
docker run --rm -v $(pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/import.py
docker run --rm -v $(pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query word --word Corona --format csv
docker run --rm -v $(pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query word --word Corona --format json
docker run --rm -v $(pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query word --word Corona coronavirus --format csv
docker run --rm -v $(pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query word --word Corona coronavirus --format csv --doctype plenarprotokoll
docker run --rm -v $(pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query top-occ
docker run --rm -v $(pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query top-occ --ngram 2
docker run --rm -v $(pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query top-occ --top 5 --offset 110
docker run --rm -v $(pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query compare --word zukunft vergangenheit