# Dashboard zur statistischen Analyse im Sächsischen Landtag
> Was wurde wie oft gesagt im Sächsischen Landtag

Das Projekt versucht die Protokolle und möglicherweise später auch alle anderen öffentlichen Dokumente im Sächsischen Landtag durchsuchbar zu machen und durch unterschiedliche Visualisierungen auch greifbar zu machen und anschaulich darzustellen.

![](img/docs/screenshot.png)

## Inspiration
Zeit Online hat für einiger Zeit dieses tolle [Tool](https://www.zeit.de/politik/deutschland/2019-09/bundestag-jubilaeum-70-jahre-parlament-reden-woerter-sprache-wandel#s=solidarit%C3%A4tszuschlag) veröffentlicht, mit dem man 70 Jahre in die Geschichte des Bundestags blicken kann. 

![](img/docs/zeit-online-analyse.png)

## Installation
Um das Tool zu installieren muss eigentlich nur Git und Docker auf dem System installiert sein.
Installationsanweisungen für `git` findet man [hier](https://git-scm.com/downloads) und für `Docker` kann man [hier](https://docs.docker.com/get-docker/) fündig werden.

Sind erstmal die Vorraussetzungen installiert, passiert alles innerhalb des Docker-Containers und somit müssen keine weiteren Abhängigkeiten installiert werden. 
Einen Datenimport kann man jetzt wie folgt machen:

```sh
docker build -t gerbsen/saxlt-minutes .
```

Möchte man das Image nicht lokal bauen, kann man auch einfach das vorkonfigurierte Image `registry.gitlab.com/gerbsen/saxlt-protokolle:develop` verwenden. 
Bitte beachten, dass spätere Beispielkommandos dann eventuell angepasst werden müssen.
Nachdem nun einmal der Import ausgeführt wurde (passiert beim Bauen des Images), kann man Anfragen gegen die „Datenbank” ausführen.
Als Hinweis: Beim Importieren sollten im `data`-Verzeichnis eine Reihe von `.txt` und `.json` Dateien entstanden sein.

## Beispielanwendungen
Grundsätzlich gelten folgenden Festlegungen für alle Operationen:
- Wörter bzw. Synonyme können mit "|" getrennt geschrieben werden, z.B.: "Corona|Covid"
- die Groß- und Kleinschreibung wird aktuell grundsätzlich ignoriert, es wird zukünftig die Möglichkeit geben dies per Parameter zu deaktivieren

Um den Import manuell zu starten kann man das Image zu aufrufen:
```sh
$ docker run --rm -v (pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/import.py
```

Um nach der Häufigkeit des Vorkommens eines Worts zu suchen:
- verwenden wir den Query-Typ: **--query word**
- verwenden wir als Wort „Corona”: **--word Corona** (Groß- und Kleinschreibung wird nicht beachtet)
- als Ausgabeformat kann zwischen `csv` und `json` ausgewählt werden **--format csv**
```sh
$ docker run --rm -v (pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query word --word Corona --format (csv|json)
```

Um nach der Häufigkeit des Vorkommens eines Worts und dessen Synonymen zu suchen:
- verwenden wir zusätzlich zu den Parametern von oben einfach mehrere Word-Parameter: **--word covid coronavirus**
- es ist ebenfalls möglich mehrere Synonym in einem Wort anzugeben. Dazu werden die Wörter mit "|" getrennt: **--word "covid|coronavirus"**
```sh
$ docker run --rm -v (pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query word --word Corona coronavirus --format csv
```

Mit dem Parameter **doctype** kann man nur in Dokumenten eines bestimmten Types suchen:
- dazu verwenden wir den Parameter **--doctype plenarprotokoll** (mehrere Angaben möglich aber noch nicht implementiert: 22.07.2020)
```sh
$ docker run --rm -v (pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query word --word Corona coronavirus --format csv --doctype plenarprotokoll
```

Um sich die häufigsten Vorkommen über alle Dokumente zu visualisieren:
- verwenden wir den Query-Typ: **--query top-occ**
- Standardlimit sind 25 Ergebnisse (für 1,2 & 3-Gramme)
```sh
$ docker run --rm -v (pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query top-occ
```

Um sich die häufigsten Vorkommen über alle Dokumente in verschiedenen **n-gram = 2** zu visualisieren:
- verwenden wir den Query-Typ: **--query top-occ**
- Standardlimit sind 25 Ergebnisse
- verwenden wir **--ngram 2**, um in den 2-grams zu suchen
```sh
$ docker run --rm -v (pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query top-occ --ngram 2
```

Um sich die häufigsten Vorkommen über alle Dokumente zu visualisieren und weiter zu spezifizieren:
- verwenden wir den Query-Typ: **--query top-occ**
- verwenden wir **--top 5**, um lediglich 5 Ergebnisse anzuzeigen
- verwenden wir **--offset 100**, um die ersten 100 Einträge zu überspringen
```sh
$ docker run --rm -v (pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query top-occ --top 5 --offset 110

          wort  occurrences
110   deutlich          264
111  situation          264
112      allen          262
113      danke          255
114       erst          254

                               wort  occurrences
110                   zurufe linken           71
111  gegenstimmen stimmenthaltungen           70
112                       märz 2020           70
113                      pallas spd           70
114           alterspräsident svend           70

                                     wort  occurrences
110                         cdu ja gottes           39
111  gegenstimmen keine stimmenthaltungen           38
112              neuhaus wartenberg linke           38
113                  alexander dierks cdu           37
114                     frank richter spd           37
```

Um die Häufigkeit von verschiedenen Wörtern miteinander zu vergleichen
- nutzen wir den Query-Typ compare: **--query compare**
- und geben mindestens zwei Wörter an
```sh
docker run --rm -v (pwd)":/saxlt" gerbsen/saxlt-minutes /saxlt/cli.py --query compare --word zukunft vergangenheit
    sitzungsnummer  zukunft  vergangenheit
0                1        3              5
1                2        1              0
2                3        8              1
3                4        0              0
4                5       32             11
5                6       30              0
6                7       22              4
7                8       16              4
8                9       26             10
9               10       26              5
10              11       29              6
```

Um den REST Server zu starten und die Requests an die API zu schicken:
```sh
docker run --rm -p 5000:5000 -v (pwd)":/saxlt" --name saxlt gerbsen/saxlt-minutes /saxlt/server.py
 * Serving Flask app "server" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
```

Und um dann eine Anfrage zu starten kann man cURL wie folgt nutzen
```sh
$ curl --silent "127.0.0.1:5000/compare?word=angst&word=zuversicht" | jq
{
  "sitzungsnummer": {
    "0": 1,
    "1": 2,
    ...
    "10": 11,
    "11": 12
  },
  "angst": {
    "0": 0,
    "1": 2,
    ...
    "10": 8,
    "11": 13
  },
  "zuversicht": {
    "0": 1,
    "1": 0,
    ...
    "10": 0,
    "11": 5
  }
}
```

Die Top-N häufig vorkommenden Wörter für Unigramme, Bigramme und Trigramme kann man wie folgt, über die REST API abfragen:
```sh
$ curl --silent "127.0.0.1:5000/top-occurrences?top=2&offset=1&doctype=plenarprotokoll" | jq
{
  "1gram": {
    "2": {
      "occurrences": 1960,
      "wort": "herr"
    },
    "3": {
      "occurrences": 1917,
      "wort": "cdu"
    }
  },
  "2gram": {
    "2": {
      "occurrences": 861,
      "wort": "vielen dank"
    },
    "3": {
      "occurrences": 785,
      "wort": "sächsischer landtag"
    }
  },
  "3gram": {
    "2": {
      "occurrences": 403,
      "wort": "landtag wahlperiode sitzung"
    },
    "3": {
      "occurrences": 372,
      "wort": "dr matthias rößler"
    }
  }
}
```

**Achtung:** Die Schlüssel der Wörter entsprechen den Schlüsseln der Sitzungsnummern.

Um per Webservice eine Graphdefinition des Vega Lite Frameworks zu erhalten, kann folgender Webservice `/graph` aufgerufen werden.
Dem Service wird eine Liste an `word` Parameter übergeben (Synonyme mittels |) und liefert als Resultat die JSON Spezifikation des Graphen. 
Aktuell ist die Liste an `word` Parameter nicht limitiert, allerdings empfiehlt sich nicht mehr als 5 zu verwenden, da sonst der Graph sehr unübersichtlich wird.

```sh
$ curl --silent "localhost:5000/graph?word=Wirtschaft&word=Sozial" | jq
```
Ergibt dann die Graph Representation in [Vega-Lite](https://vega.github.io/vega-lite/). Man kann dann auch im [Online-Editor von Vega](https://vega.github.io/editor/) mit den Werten noch etwas spielen.
```json
{
  "$schema": "https://vega.github.io/schema/vega-lite/v4.8.1.json",
  "config": { "view": { "continuousHeight": 300, "continuousWidth": 400 } },
  "data": {
    "name": "data-052b3e8e681541502ab85f7e96e35249"
  },
  "datasets": {
    "data-052b3e8e681541502ab85f7e96e35249": [
      {
        "Sozial": 0,
        "Wirtschaft": 0,
        "sitzungsnummer": 1
      },
      // ... Teile rausgelassen 
      {
        "Sozial": 2,
        "Wirtschaft": 25,
        "sitzungsnummer": 13
      }
    ]
  },
  "encoding": {
    "color": { "field": "key", "title": "Wörter", "type": "nominal" },
    "column": { "field": "sitzungsnummer", "title": "Sitzungsnummer", "type": "nominal" },
    "x": { "field": "key", "title": "", "type": "ordinal" },
    "y": { "field": "value", "title": "Häufigkeit", "type": "quantitative" }
  },
  "mark": "bar",
  "transform": [ { "fold": [ "Wirtschaft", "Sozial" ] } ]
}
```

## Release History

* 0.0.1
  * Work in progress, Möglichkeit zum Suchen von Häufigkeiten, Top-Häufigkeiten und der Ausgabe mittels Vega-Lite mit Docker und HTML

## Meta

Dr. Daniel Gerber – [@gerbsen](https://twitter.com/gerbsen) – team@danielgerber.eu

Veröffentlich unter MIT Lizenz. Unter ``LICENSE`` findet man dazu mehr Informationen

[https://gitlab.com/gerbsen/saxlt-protokolle](saxlt-protokolle)

## Wie kann man mitmachen?
Wenn du mitmachen möchtest, wäre es erstmal das einfachste du legst einen Issue im Projekt an.
Dort können wir dann besprechen, wie wir mit deiner Anfrage umgehen. 
Oder probierst es einfach und erzeugst einen Merge Request ;)

1. Forke das Repository
2. Erzeuge einen neuen Feature-Branch
3. Commite deine Änderungen
4. Pushe das zu einem neuen Branch
5. Erzeuge einen Merge Request

