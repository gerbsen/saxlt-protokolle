# Alle Abgeordneten der 7. Legislatur

Anton, Rico
Barth, André
Beger, Mario
Böhme, Marco
Breitenbuch, Georg-Ludwig von
Brünler, Nico
Buddeberg, Sarah
Cagalj Sejdi, Petra
Dierks, Alexander
Dietrich, Eric
Dombois, Andrea
Dornau, Jörg
Dringenberg, Dr. Volker
Dulig, Martin
Feiks, Antje
Firmenich, Iris
Flemming, Ingo
Friedel, Sabine
Fritzsche, Oliver
Gahler, Torsten
Gasse, Holger
Gebhardt, Rico
Gemkow, Sebastian
Gerber, Dr. Daniel
Gorskih, Anna
Hahn, Christopher
Hammecke, Lucie
Hartmann, Christian
Hein, René
Heinz, Andreas
Hentschel, Holger
Hippold, Jan
Homann, Henning
Hösl, Stephan
Hütter, Carsten
Jost, Martina
Keil, Wolfram
Keiler, Dr. Joachim Michael
Keller, Tobias
Kiesewetter, Jörg
Kirmes, Svend-Gunnar
Kirste, Thomas
Klepsch, Barbara
Kliese, Hanka
Köditz, Kerstin
Kretschmer, Michael
Kuge, Daniela
Kuhfuß, Kathleen
Kühne, Jörg Steffen
Kuhnert, Roberto
Kummer, Ines
Kumpf, Mario
Kuppi, Lars
Lang, Simone
Leithoff, Susan
Liebscher, Gerhard
Lippmann, Valentin
Löffler, Jan
Löser, Thomas
Lupart, Ulrich Willi
Mackenroth, Geert W.
Maicher, Dr. Claudia
Mann, Holger
Markert, Jörg
Mayer, Norbert Otto
Melcher, Christin
Mertsching, Antonia
Meyer, Dr. Stephan
Mikwauschk, Aloysius
Modschiedler, Martin
Nagel, Juliane
Neuhaus-Wartenberg, Luise
Nicolaus, Kerstin
Nowak, Andreas
Oberhoffner, Jens
Otto, Gerald
Pallas, Albrecht
Panter, Dirk
Patt, Peter Wilhelm
Penz, Romy
Peschel, Frank
Petzold, Gudrun
Piwarz, Christian
Pohle, Ronald
Prantl, Thomas
Richter, Frank
Ritter, Kay
Rohwer, Lars
Rößler, Dr. Matthias
Rost, Wolf-Dietrich
Saborowski, Ines
Schaper, Susanne
Schaufel, Dietmar Frank
Schenderlein, Dr. Christiane
Schiemann, Marko
Schmidt, Thomas
Schreyer, Timo
Schubert, Franziska
Schultze, Mirko
Schwietzer, Doreen
Sodann, Franz
Springer, Ines
Tändler-Walenta, Marika
Teichmann, Ivo
Thumm, Thomas
Ulbrich, Roland Walter Hermann
Urban, Jörg
Voigt, Sören
Wähner, Ronny
Weigand, Dr. Rolf
Wendt, André
Wiesner, Alexander
Winkler, Volkmar
Wippel, Sebastian
Wissel, Patricia
Wöller, Prof. Dr. Roland
Zickler, Hans-Jürgen
Zschocke, Volkmar
Zwerg, Jan-Oliver 