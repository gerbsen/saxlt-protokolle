import re

def get_window(sentence, phrase, window_size):
  sentence = sentence.split()
  phrase   = phrase.split()
  words    = len(phrase)
  result   = []
  for i, word in enumerate(sentence):
    if word == phrase[0] and sentence[i:i + words] == phrase:
      start = max(0, i - window_size)
      result.append(' '.join(sentence[start:i + words + window_size]))
  return result

def find_matching_lines(lines):
  results = []
  for index, line in enumerate(lines):
    if "highlighted-match-queryString" in line:
      results.append(index + 1)
  return results

def highlight_text(line, searchString):
  
  for name in get_bgr_names():
    line = line.replace(name, "<span class='p-2 highlighted-match-bgr highlighted-match-party'>" + name + "</span>")
  for name in get_linke_names():
    line = line.replace(name, "<span class='p-2 highlighted-match-linke highlighted-match-party'>" + name + "</span>")
  for name in get_spd_names():
    line = line.replace(name, "<span class='p-2 highlighted-match-spd highlighted-match-party'>" + name + "</span>")
  for name in get_afd_names():
    line = line.replace(name, "<span class='p-2 highlighted-match-afd highlighted-match-party'>" + name + "</span>")
  for name in get_cdu_names():
    line = line.replace(name, "<span class='p-2 highlighted-match-cdu highlighted-match-party'>" + name + "</span>")
  
  return re.sub(searchString, "<span class='highlighted-match-queryString'>" + searchString + "</span>", line, flags=re.IGNORECASE)

def get_all_names():
  return get_bgr_names() + get_linke_names() + get_spd_names() + get_afd_names() + get_cdu_names()

def get_bgr_names_mdl():
  return ["Petra Sejdi Cagalj, BÜNDNISGRÜNE", "Dr. Daniel Gerber, BÜNDNISGRÜNE", "Lucie Hammecke, BÜNDNISGRÜNE", "Kathleen Kuhfuß, BÜNDNISGRÜNE", "Ines Kummer, BÜNDNISGRÜNE", "Gerhard Liebscher, BÜNDNISGRÜNE", "Valentin Lippmann, BÜNDNISGRÜNE", "Thomas Löser, BÜNDNISGRÜNE", "Dr. Claudia Maicher, BÜNDNISGRÜNE", "Christin Melcher, BÜNDNISGRÜNE", "Franziska Schubert, BÜNDNISGRÜNE", "Volkmar Zschocke, BÜNDNISGRÜNE"]

def get_bgr_names_gov():
  return ["Wolfram Günther", "Katja Meier"]

def get_bgr_names():
  return get_bgr_names_gov() + get_bgr_names_mdl()

def get_linke_names():
  return ["Marco Böhme, DIE LINKE", "Nico Brünler, DIE LINKE", "Sarah Buddeberg, DIE LINKE", "Antje Feiks, DIE LINKE", "Rico Gebhardt, DIE LINKE", "Anna Gorskih, DIE LINKE", "Kerstin Köditz, DIE LINKE", "Antonia Mertsching, DIE LINKE", "Juliane Nagel, DIE LINKE", "Luise Neuhaus-Wartenberg, DIE LINKE", "Susanne Schaper, DIE LINKE", "Mirko Schultze, DIE LINKE", "Franz Sodann, DIE LINKE", "Marika Tändler-Walenta, DIE LINKE"]

def get_spd_names():
  return ["Martin Dulig, SPD", "Sabine Friedel, SPD", "Henning Homann, SPD", "Hanka Kliese, SPD", "Simone Lang, SPD", "Holger Mann, SPD", "Albrecht Pallas, SPD", "Dirk Panter, SPD", "Frank Richter, SPD", "Volkmar Winkler, SPD"]

def get_afd_names():
  return ["André Barth, AfD", "Mario Beger, AfD", "Jörg Dornau, AfD", "Dr. Volker Dringenberg, AfD", "Torsten Gahler, AfD", "Christopher Hahn, AfD", "René Hein, AfD", "Holger Hentschel, AfD", "Carsten Hütter, AfD", "Martina Jost, AfD", "Wolfram Keil, AfD", "Dr. Joachim Keiler, AfD", "Dr. Joachim Michael Keiler, AfD", "Tobias Keller, AfD", "Thomas Kirste, AfD", "Jörg Steffen Kühne, AfD", "Roberto Kuhnert, AfD", "Mario Kumpf, AfD", "Lars Kuppi, AfD", "Ulrich Willi Lupart, AfD", "Norbert Otto Mayer, AfD", "Jens Oberhoffner, AfD", "Romy Penz, AfD", "Frank Peschel, AfD", "Gudrun Petzold, AfD", "Thomas Prantl, AfD", "Dietmar Frank Schaufel, AfD", "Timo Schreyer, AfD", "Doreen Schwietzer, AfD", "Ivo Teichmann, AfD", "Thomas Thumm, AfD", "Roland Walter Hermann Ulbrich, AfD", "Jörg Urban, AfD", "Dr. Rolf Weigand, AfD", "André Wendt, AfD", "Alexander Wiesner, AfD", "Sebastian Wippel, AfD", "Hans-Jürgen Zickler, AfD", "Jan-Oliver Zwerg, AfD"]

def get_cdu_names():
  return ["Rico Anton, CDU", "Georg-Ludwig von Breitenbuch, CDU", "Alexander Dierks, CDU", "Eric Dietrich, CDU", "Andrea Dombois, CDU", "Iris Firmenich, CDU", "Ingo Flemming, CDU", "Oliver Fritzsche, CDU", "Holger Gasse, CDU", "Sebastian Gemkow, CDU", "Christian Hartmann, CDU", "Andreas Heinz, CDU", "Jan Hippold, CDU", "Stephan Hösl, CDU", "Jörg Kiesewetter, CDU", "Svend-Gunnar Kirmes, CDU", "Barbara Klepsch, CDU", "Michael Kretschmer, CDU", "Daniela Kuge, CDU", "Susan Leithoff, CDU", "Jan Löffler, CDU", "Geert W. Mackenroth, CDU", "Jörg Markert, CDU", "Dr. Stephan Meyer, CDU", "Aloysius Mikwauschk, CDU", "Martin Modschiedler, CDU", "Kerstin Nicolaus, CDU", "Andreas Nowak, CDU", "Gerald Otto, CDU", "Peter Wilhelm Patt, CDU", "Christian Piwarz, CDU", "Ronald Pohle, CDU", "Kay Ritter, CDU", "Lars Rohwer, CDU", "Dr. Matthias Rößler, CDU", "Wolf-Dietrich Rost, CDU", "Ines Saborowski, CDU", "Dr. Christiane Schenderlein, CDU", "Marko Schiemann, CDU", "Thomas Schmidt, CDU", "Ines Springer, CDU", "Sören Voigt, CDU", "Ronny Wähner, CDU", "Patricia Wissel, CDU", "Prof. Dr. Roland Wöller, CDU"]