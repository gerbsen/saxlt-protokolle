#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import glob, sys
import json
import pandas
import altair as altair
import logging as log
import time
from collections import Counter

def getTopOccurrences(top=25, offset=0, doctype=['plenarprotokoll']):
  
  total1Gram = None
  total2Gram = None
  total3Gram = None
  start = time.time()
  for filePath in [f for f in glob.glob("/saxlt/data/*.json")]: 
    with open(filePath, 'r') as dataJsonFile:
      data = json.loads(dataJsonFile.read())
      if ( data['type'] in doctype ):
        total1Gram = total1Gram + Counter(data['1_gram_occurrences']) if total1Gram is not None else Counter(data['1_gram_occurrences'])
        total2Gram = total2Gram + Counter(data['2_gram_occurrences']) if total2Gram is not None else Counter(data['2_gram_occurrences'])
        total3Gram = total3Gram + Counter(data['3_gram_occurrences']) if total3Gram is not None else Counter(data['3_gram_occurrences'])

  total1Gram = dict(Counter(total1Gram))
  total2Gram = dict(Counter(total2Gram))
  total3Gram = dict(Counter(total3Gram))
  end = time.time()
  log.info("It took %.2fms to read the json files.", end - start)

  result = {}
  pandas.set_option("max_rows", None)
  sortedData = sorted(total1Gram.items(), key=lambda x: x[1], reverse=True)
  df = pandas.DataFrame(sortedData, columns =['wort', 'occurrences']) 
  result['1gram'] = df.iloc[offset:top+offset,:]
  result['1gram'].index += 1
  sortedData = sorted(total2Gram.items(), key=lambda x: x[1], reverse=True)
  df = pandas.DataFrame(sortedData, columns =['wort', 'occurrences']) 
  result['2gram'] = df.iloc[offset:top+offset,:]
  result['2gram'].index += 1
  sortedData = sorted(total3Gram.items(), key=lambda x: x[1], reverse=True)
  df = pandas.DataFrame(sortedData, columns =['wort', 'occurrences']) 
  result['3gram'] = df.iloc[offset:top+offset,:]
  result['3gram'].index += 1
  return result

def getOccurrences(words, ngram=1):
  # loop over all words in the query
  words = set([x.lower() for x in words])
  occurrences = []
  # loop over all protocolls in the directory
  for filePath in [f for f in glob.glob("/saxlt/data/*.json")]: 
    # read the json data from import
    with open(filePath, 'r') as dataJsonFile:
      data = json.loads(dataJsonFile.read())        
      counter = 0;
      for word in words:
        # synonyms are written as "rechts|rechtsextrem|nazi"
        splitWords = [x.strip() for x in word.split('|')]
        for splitWord in splitWords:
          if splitWord in data[str(ngram) + '_gram_occurrences']:
            counter += data[str(ngram) + '_gram_occurrences'][splitWord]
      occurrences.append({ 
        'sitzungsnummer': data['sitzungsnummer'],
        'count': counter
      })
  return sorted(occurrences, key=lambda k: k['sitzungsnummer'])

def compare(words):
  firstWord = words.pop(0)
  dataframe = pandas.DataFrame(getOccurrences([firstWord]))
  dataframe.rename(columns={'count': firstWord}, inplace=True)
  for word in words:
    occurrences = getOccurrences([word])
    result = pandas.DataFrame(occurrences)
    result.rename(columns={'count': word}, inplace=True)
    dataframe = dataframe.join(result.set_index('sitzungsnummer'), on="sitzungsnummer")
  return dataframe

def compare_graph_from_vega_json(words):
  chart = altair.Chart(
    pandas
      .DataFrame(compare(words.copy()))) \
      .transform_fold(words) \
      .mark_bar() \
      .encode(
          altair.X('key:O', title=""), 
          altair.Y('value:Q', title="Häufigkeit"),
          altair.Column('sitzungsnummer:N', title="Sitzungsnummer"),
          altair.Color('key:N', title="Wörter")
      )
  return chart.to_json()

def toHTML(query):
  if query['type'] == "word":
    chart = altair \
      .Chart(data=pandas.DataFrame(getOccurrences(query['words'])), title=query['title']) \
      .mark_bar() \
      .encode( 
        altair.X(query['x']['id'], title=query['x']['title']), 
        altair.Y(query['y']['id'], title=query['y']['title'])
      ) \
      .properties(
        width='container'
      )
    return chart.to_json()
  elif query['type'] == "compare":
    chart = altair.Chart(
      pandas
        .DataFrame(compare(['angst', 'vertrauen']))) \
        .transform_fold(
          ['angst', 'vertrauen']
        ) \
        .mark_bar() \
        .encode(
            altair.X('key:O', title=""), 
            altair.Y('value:Q', title="Häufigkeit"),
            altair.Column('sitzungsnummer:N', title="Sitzungsnummer"),
            altair.Color('key:N', title="Wörter")
        )
    return chart.to_json()
  else:
    print('Error: query type not known') 
    return None
